# Organization

Organisatorisches für den Kurs WWI22AMB. Zusammen haben wir 55 VE, welche wir in 11 Sessions zu je 5 VE aufgeteilt haben.

## Prüfungsleistung

Insgesamt verteilen wir in diesem Kurs 100 Punkte. Diese setzen sich zusammen aus:
- 10 Challenges zu je 10 Punkten, also 100 Punkte
- 10 Bonus-Punkte durch die Kahoots

### Challanges

Jede Session bekommt ihr von uns eine Challenge, welche bis zur nächsten Session zu erledigen ist. Diese findet ihr in `./challenges`.
Die Challenges sind in Teams zu bearbeiten und werden pro Team benotet.

### Kahoots

Jede Session fragen wir die Inhalte aus der vorherigen Session spielerisch mit einem Kahoot ab.
Wer am Ende des Kurses mind 70% aller Fragen richtig beantwortet hat, kriegt 10 Bonus-Punkte.
Wer am Ende des Kurses die meisten Fragen richtig beantwortet hat, kriegt Swag der diesjährigen [KubeCon](https://events.linuxfoundation.org/kubecon-cloudnativecon-europe/).

## Wie wir arbeiten

Dieser Kurs wird teils remote und teils vor Ort veranstaltet. Zur asynchronen Kommunikation benutzen wir Slack. Aufgaben und Abgaben dokumentieren wir hier auf Gitlab.

Wir lassen euch viele Freiheiten bzgl. wie und wann ihr arbeitet. Wir erwarten von euch ausreichend Selbstständigkeit,
um die Arbeit im vorgegebenen Zeitrahmen zu erledigen und gleichmäßig in eurem Team aufzuteilen.

Lest mehr in unserem `./CODE_OF_CONDUCT.md`.
