# Challenge 9

Deadline: 2024-04-18 12:00 MESZ

## Part 1: Too many APIs!!! (4 pts)

In your absence, the big german bank has implemented several microservices and exposed them to more clients, including external clients such as Check24.
But it is now starting to struggle to understand what clients consume which services and which services consume other services.
Additionally, many services implement authentication and rate limiting differently, making it complicated for developers to consume multiple services in their apps.

Come up with a new architectural design that incorporates an API Management solution to solve these problems.
Try to answer following questions:

- How would your API Management solution be integrated in the bank's new system landscape?
- What responsibilities would the API Gateway have?
- Would you differentiate between external API consumers and internal consumers? If yes, how? How would each access the APIs?
- What are the trade-offs of your solution?
- What would be the best way to migrate from the current architecture to your architecture? What timeline would you suggest?

Modify the `banking-architecture.png` diagram (of this challenge) accordingly. You can use Excalidraw and the `banking-architecture.excalidraw` source file.

Prepapre a form-free 5 minute presentation of your solution for the session on 2024-04-18 and be ready to answer questions about it.

## Part 2: Set up an API Gateway for Zwitscher (6 pts)

The Zwitscher backend has been deployed to Google Cloud Run. You are now tasked to update your Zwitscher frontend to utilize this new cloud-based backend.
Note that there have been some changes to how the API works. See the new [zwitscher-cloud] repository for more information.

But we now want to use our knowledge of API Management to move some capabilities to a central component.

For your team, create and deploy an API Gateway that abstracts requests to the cloud-based Zwitscher backend and does the following:

- Rate limit `POST /zwitscher` requests. Users are allowed to create a most one Zwitscher per minute (based on their IP address)
- Set CORS response headers
- Ensure that only authenticated users can perform `POST /zwitscher` requests
- Validate that the author of a Zwitscher matches the user ID / email of the authenticated user

Your API Gateway needs to expose a publicly accessible URL endpoint that fulfills above requirements.

Your frontend now has to make requests to the API Gateway instead of the local backend.
Modify your Zwitscher frontend accordingly and remove the code for the backend and the database.
As before, running `docker compose up` needs to start the frontend and all existing functionalities of the frontend must continue to work.

We recommend to use [Zuplo](https://zuplo.com) as API Management solution. It's fast, developer-friendly and comes with a free tier that will be sufficient for these requirements.

Note: Because all teams will deploy API Gateways that use the same Zwitscher backend, you might see Zwitschers that have been created by other teams.