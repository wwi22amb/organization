# Challenge 10: Cloud Exit (10 pts)

Deadline: 2024-04-22 12:00 MESZ

Your boss saw the costs of the cloud-based Zwitscher backend from last week's challenge
and thinks that they can do better hosting the backend on their own.
You are tasked to deploy the backend to a self-hosted Kuberneters cluster called [dhbw-cluster](https://gitlab.com/wwi22amb/dhbw-cluster).

Fork the [zwitscher-cloud](https://gitlab.com/wwi22amb/zwitscher-cloud) repository to your team's Gitlab group.
Create a Gitlab CI/CD pipeline that automatically (on every commit) builds the Docker image for the backend,
stores it in Gitlab Container Registry and then deploy that image as a pod to the Kubernetes cluster to your team's namespace.

The backend service must be usable (create and read Zwitschers)
and accessible under `https://dhbw.gemueseoderobst.de/<team>/zwitscher-service`,
for example `https://dhbw.gemueseoderobst.de/team5/zwitscher-service`. 

You will need credentials to deploy to the cluster which you will get from us when you ask us on Slack.

Note that your credentials MUST NOT get comitted to the source code of your repository. Use CI/CD variables instead.
We will deduct points for credentials we find in your source code history.

You can find the deployment configuration for a dummy service here: [dhbw-cluster-dummy-service](https://gitlab.com/wwi22amb/dhbw-cluster-dummy-service).
