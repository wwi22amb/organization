# Challenge 5

## Deadline

2024-03-21 12:00 CET

## Part 1 - Banking Architecture and Containers (2 pts)

The big german bank noticed there's some new kid on the block: Containers! ✨

They heard from other banks and financial institutes that containers are solving a lot of problems and make running IT Services much cheaper.

Now they want you to use containers whereever possible!

in the new `banking-architecture` repository, prepare a short presentation (5 min) answering the following questions:

- Name three advantages of containers over VMs.
- Which of your services in your banking-architecture are feasible for the deployment in a container?
- How would you handle many container-based services, manage their secrets and needs for persistent storage?
  - Are there any solutions out there which could help you? If yes, which one would you choose for your banking-architecture and why?

## Part 2 - Containerize your application (8 pts)

Take your trading-api service and containerize it. You can reuse the old repository for this challenge.

You need to fulfill the following requirements:

- Service is containerized and runs on your local machine
- Service is accessible from your local machine
- The Database dependency (MySQL, Postgres, whatever) is also containerized, runs on your local machine and is accessible
    - Note: this time, you are not allowed to use a database hosted in the cloud somehwere. We want you to get it to run locally.
- Repository contains instructions on how to build and run the newly containerized service
- The repository contains a `docker-compose.yaml` file that can be used to start the service
  - The compose file also contains settings for health checks

## Additional notes

If your service needs credentials to run, make sure to NOT STORE THEM IN CODE.
It is never a good idea to store secrets in code. Instead, figure out how to
provide your service with the credentials securely. For example, using
environment variables.
