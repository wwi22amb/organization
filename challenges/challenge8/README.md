# Challenge 8

Deadline: 2024-04-11 12:00 MESZ

## Part 1: Let your Zwitscher users login (5 pts)

You are now tasked to implement a login mechanism for the Zwitscher users. Users are supposed to be able to log in through third-party
identity providers (such as Google log in or Gitlab log in).

Adapt your Zwitscher frontend from last week's challenge to fulfill following requirements.

- Users have the possibility to log in, either with username / email and password or through a third-party identity provider
- Only logged in users can create new Zwitschers
- All users can see all Zwitschers

As an all-in-one identity solution, we recommend using Google Cloud Identity Platform.
It directly integrates with third-party identity providers (Google, Apple, LinkedIn, etc.) and manages your user identities for you.
It's free, but reach out to us if you want us to create another Google Cloud project for you.

However, there are many alternatives out there if you're feeling adventurous.

Commit your changes to the frontend the Zwitscher repository. As before, running `docker compose up` needs to start all components of the
Zwitscher app. If not obvious or log in is only possible with a test account, provide instructions on how to log in on your site in the README.

## Part 2: Secure your backend (5 pts)

So far you have only restricted Zwitscher creation in your frontend, but that does not keep unauthenticated users from directly
performing requests to your Zwitscher backend using tools like Postman. You now need to protect your API, so that only authenticated users
can use the `POST /zwitschers` endpoint.

Requirements:
- The `POST /zwitschers` endpoint validates that only users that logged in in your frontend can create Zwitschers.
- The `GET /zwitschers` endpoint does not require any authentication.
- You adapt the internal data model of the backend to also store the user IDs of authors alongside Zwitschers.
- The `GET /zwitschers` now returns the author's user ID for each Zwitscher.
- You modify the OpenAPI specification accordingly.

Note that the current backend is implemented in Golang. Feel free to rewrite the backend in a different language if you feel like it
and as long as you comply with the API specification. ChatGPT is supposedly good at stuff like that.

In case you are using cookie-based authentication, make sure your backend is not subsceptible to CSRF attacks.
If you need a refresher on that, see [here](https://victorzhou.com/blog/csrf/).

Commit your changes to the frontend the Zwitscher repository. As before, running `docker compose up` needs to start all components of the
Zwitscher app.
