# Challenge 6

## Deadline

2024-03-28 12:00 CET

## Part 1 - Automation is key - 3 pts

The board of directors observed that some of our competitors achieve a faster
time-to-market than we do, despite our utilization of containers and container
orchestration. This is impacting revenue and consequently jeopardizing our
chances of receiving a raise. Action needs to be taken.

In the upcoming weeks, the bank aims to implement the following features:

- Introduce a new cashback card, enabling customers to save money while
  shopping online. Cashbacks will be promptly credited to their main checking
  account.
- Enable customers to invest in index funds on a monthly basis. These
  investments will occur twice a month for all customers, based on their
  preferences, either on the first or the 15th.

The previously proposed infrastructure cannot accommodate these requirements as
it operates in a production environment. We need to ensure that in the event of
increased demand due to the success of the new products, the existing
infrastructure remains stable and separated.

The developers require test environments starting next week. This will enable
the cashback department to thoroughly test the features and ensure a smooth
release without any bugs.

Your task is to design an architecture overview that can seamlessly handle both
use cases while integrating with existing services from the previous
architecture. Additionally, the board of directors expects a plan outlining how
you intend to construct the new infrastructure and ensure a faster
time-to-market compared to our competitors.

Prepare a 5-minute presentation detailing your architecture overview and
proposal to address the need for a faster time-to-market.

## Part 2 - Deploy Deploy Deploy Deploy Deploy - 7 pts

Deploy your old existing `spread-api` service to Google Cloud Run using Gitlab
CI. Write a Gitlab CI Pipeline where you ensure the application's functionality
and subsequently deploy it to Google Cloud Run. Ensure that credentials are not
inserted into the Gitlab CI YAML file.
