# Challenge 4

Deadline: 2024-03-14 12:00 MEZ

## Part 1: Make your banking architecture cloud-ready (6 pts)

The big german bank you are consulting wants to move its new banking systems into the public cloud.
They don't want to maintain a data center on their own anymore after years of contracting IBM to do it for them.

They task you to find a suitable cloud provider (or multiple cloud providers?).

Based on your final banking architecture from challenge 3, try to figure out where to host all of these components.

In your `banking-architecture` repository, create a new Markdown file called `cloud.md`
and write a text document answering following questions:

1. Which cloud provider(s) will you choose and why?
2. What product offerings will you choose? Name the products and explain why you think these products fit your requirements most.
    - IaaS: VMs? Storage disks?
    - PaaS: Databases? Serverless products? Managed Kubernetes?
    - For example: _For our Transactions Database we might be using Google Cloud Firestore because it's a managed Document database that can scale easily, is performant and reduces maintenance overhead. But you are paying per transaction, so in high volumes it might not make sense._
3. Do you have any risk of vendor lock-in and if so, how could you reduce that risk?
4. Will the products be performant enough and scale to your use case?
    - For example: Running all infrastructure on a single virtual machine might not result in the performance you need for your banking systems.
5. In what regions will your data be stored?
6. What will all of this cost? (roughly)
    - Consider computing, storage and networking costs. Or costs of your managed services.

To answer questions about scalability, performance and costs, consider the expected load on the systems:

```
2 million customers with roughly 2.5 million accounts, but adjust for customers and accounts that might be coming in the future.
2.4 billion transactions processed so far, with up to 1.4 million new transactions per day.
In total, customers perform on average 20 million requests per day against the systems through their apps (Online Banking & Mobile App).
On month start and end, the bank expects up to 10 times more traffic on its systems than ususal. The systems need to be able to scale for that.
```

This challenge requires some research on your side. Explore the product offerings of different cloud providers
and go with what you think might work best.

Some inspirational input:
- Hyperscalers: AWS, Google Cloud, ~~Azure~~ (don't use Azure, seriously)
- Edge cloud providers: Cloudflare, Fastly
- Managed services providers: Supabase, Fly.io, Netlify
- IaaS / Cloud providers: Hetzner, DigitalOcean

Prepapre a form-free 5 minute presentation of your cloud decisions for our next session and be ready to answer questions about it.

Tip: Don't forget to create a `challenge3` tag in your repository before starting with this one.

## Part 2: Deploy a serverless function (4 pts)

You need to deploy a serverless function to the cloud. It must implement the following requirements:

- The function gets invoked per HTTP request from the Internet.
- The function accepts the symbol of a cryptocurrency (for example `BTC`) as request parameter (example: `?symbol=BTC`).
- The function checks the current prices of at least two cryptocurrency exchanges or lists (see examples below)
  and calculates the spread (meaning the difference in the prices).
- The function returns that spread to the client that invoked it.

How the HTTP response looks like is up to you.

You could choose from follwing list of price providers:

```
- Binance: Live-Daten für 100+ Kryptowährungen.
- Kraken: Live-Daten für 50+ Kryptowährungen.
- Coinmarketcap: Neartime-Daten für 8000+ Kryptowährungen von 300+ Börsen.
- Coingecko: Live-Daten für 3000+ Kryptowährungen von 400+ Börsen.

(from https://gitlab.com/19amb/mammon/organisation/-/blob/main/RESOURCES.md)
```

Create a new repository called `spread-api`. Maintain your code there.

Deploy the serverless function as a Google Cloud function. We will provide you with credentials for a Google Cloud project
where you can deploy your function into.

After the function has been deployed and can be reached from the Internet, add the function's URL to your repo's README.

IMPORTANT: If your function needs API credentials of the underlying cryptocurrency APIs it consumes, DO NOT STORE THEM IN CODE.
It is never a good idea to store secrets in code (see [AWS best practices](https://maturitymodel.security.aws.dev/en/2.-foundational/dont-store-secrets-in-code/)).
Instead, figure out how to provide secrets to your function (see [docs](https://cloud.google.com/functions/docs/configuring/secrets?hl=de)).
If we find valid secrets in your commit history, we will redact you one point per secret.

Since Google Cloud functions can incur costs when used extensively and we don't want to end up on https://serverlesshorrors.com,
we are deploying a billing killer that shuts down all Google Cloud projects if they surpass a certain spending limit.
Still, please use these cloud resources responsibly and don't incur unnecessary costs.