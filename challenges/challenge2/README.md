# Challenge 1

Deadline: 2024-02-29 12:00 MEZ

## Part 1: Create a high-level system architecture (6 pts)

You are IT consultants currently working for a large german bank. Your customer asks you to come up with an architecture for a greenfield large-scale banking IT landscape.

> Greenfield means that you don't need to worry about existing systems and infrastructure. Everything needs to be done from scratch.

In this first iteration of the architectural design, think about what components you would introduce to meet the customer's requirements (see requirements below).

Draw an architectural diagram (you can use UML diagrams if you like to).
Write a document that explains your architecture. Try to answer:

- What data resources are there (for example customer accounts)?
- What (micro)services would you introduce and what are their responsibilities?
- How do these services play together to implement the business processes (for example, what happens when a customer opens an account)?
- What trade-offs did you make (if any) and justify your decision making.

```
Functional requirements:

- The system can store the amount of money of customers in their bank accounts.
- The system can store customer data such as name, address, etc.
- The system can serve multiple end-user facing clients (Online Banking Website, Mobile App, ATMs)
- Customers can post transactions that transfer money from one bank account to another.
- New customers can open new accounts through the Online Banking.
- The system generates a monthly income statement for each customer.

Non-functional requirements:
- Customer data and customer accounts are the responsibility of different organizational domains and they need to be able to develop these systems indepdently.
- Data consistency is of utmost importance for accounts and their transactions. There must be no margin of error when dealing with account values.

Out of scope:
- Don't worry about databases or data models right now. You only need to think about what data objects there are and where they get maintained.
- Don't make any assumptions about the underlying infrastructure yet.
```

> Remember, this is a thinking exercise. There is no right or wrong solution here. We want you to become creative and think about potential solutions.

Create a new repository in your Gitlab group called `banking-architecture` and commit and push your architectural documemtation there. Use following formats:
- Image file (JPG / PNG / other) for the diagram. To initially draw and export the diagram, you can use free web-based drawing tools like [draw.io](https://app.diagrams.net/) or [Excalidraw](https://excalidraw.com/), but don't have to.
- Markdown file for the text document. [Markdown](https://www.markdownguide.org/getting-started/) is the syntax commonly used for Git repository READMEs.

Prepapre a form-free 5 minute presentation of your architecture for our next session and be ready to answer questions about it.

## Part 2: Create an API (4 pts)

A few years ago, we hosted a trading bot competition at DHBW. Located at `challenges/challenge2/trading-api.yaml` is the (reduced) OpenAPI specification for our Paper Trading API.
We want you to create an webservice that implements this API. It should be a dummy implementation, meaning it does not need to store or serve real data.
For example, if a client sends a valid request to the `POST /orders` endpoint, the API should return the response documented in the OpenAPI specification filled with example values:

```
{
  "orderID": "string",
  "assetType": "STOCK",
  "symbol": "AAPL",
  "amount": 1,
  "side": "BUY",
  "orderType": "MARKET",
  "quote": 1123.22,
  "total": 2246.44,
  "placedAt": "2021-02-01T18:07:00",
  "expiresAfter": "2021-02-01T18:07:00"
}
```

Also, pay attention to headers that the client needs to provide and to your HTTP status codes
(for example, if there are no `x-user-id` and `x-user-secret` headers provided in the request, the API should respond with status `401 Unauthorized`).

Useful tools:
- Visualize the OpenAPI specification with [Swagger Editor](https://editor.swagger.io/) (just copy and paste it into there)
- [Postman](https://www.postman.com/) to make HTTP requests against your API

You are free to create the webservice with whatever programming language and framework you like. For example:
- [Node.js](https://nodejs.org/en) with [hapi](https://hapi.dev/) (recommended)
- [Python](https://www.python.org/) with [Flask](https://flask.palletsprojects.com/en/3.0.x/quickstart/)
- [Go](https://go.dev/) with [Gin-Gonic](https://gin-gonic.com/)

Create a new repository called `trading-api` and commit and push your webservice.
Your repository's README needs to come with detailed instructions on how to run the webservice (for example, see the [hello-http-server](https://gitlab.com/wwi22amb/hello-http-server) repository).

Within the next few days we will provide you with a way to automatically test whether your implementation fully adheres to the provided OpenAPI specification.
