# Challenge 7

Deadline: 2024-04-04 12:00 MEZ

## Part 1: Build the Zwitscher Frontend (6 pts)

You have been tasked with building Zwitscher, an open-source alternative to the other social network with a bird which got bought by some
lunatic who started to randomly pull on plugs of server racks.

Some work has already been done: There exists a backend API that allows you to create and read Zwitscher posts and stores them in a database.
See [https://gitlab.com/wwi22amb/zwitscher](https://gitlab.com/wwi22amb/zwitscher).

Your task for now is to build a prototype for a web frontend. Fork the Zwitscher repository into your team's group and create a new folder called `frontend` in it where you will maintain the code for the website.

Choose whatever technology you like. Think about whether server-side or client-side rendering would make more sense and implement it accordingly.
You also need to create a containerized web server that serves the website so that your testers can start the whole system as easily as possible.

Requirements:

(6 pts)
- Users can use the website to create new Zwitschers and read all other existing Zwitschers.
- The website communicates with the backend API to do so.

(4 pts)
- Your website startup configuration has been included in the docker-compose setup. Running `docker compose up` will start the webserver, the backend and the database.
- When started through docker-compopse, the website can be reached on the local machine at `localhost:8080`.

Note: You will most likely run into troubles with CORS. You may need to adapt the backend API to set the correct CORS headers. Learn more about CORS here: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
